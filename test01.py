import networkx as nx
import matplotlib.pyplot as plt

NODES = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'
]

EDGES = [
    ('a', 'b'),
    ('b', 'c'), ('b', 'e'), ('b', 'f'),
    ('c', 'g'), ('c', 'd'),
    ('d', 'c'), ('d', 'h'),
    ('e', 'a'),
    ('f', 'g'),
    ('h', 'g'), ('h', 'd')
]


if __name__ == '__main__':
    G = nx.DiGraph()
    G.add_nodes_from(NODES)
    G.add_edges_from(EDGES)

    nx.draw(G, with_labels=True)
    plt.draw()
    plt.show()
