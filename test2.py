import networkx as nx

NODES = [
    'a', 'b', 'c', 'd', 'e'
]

EDGES = [
    ('a', 'c'), ('a', 'd'),
    ('b', 'a'),
    ('c', 'b'),
    ('d', 'e')
]


def dfs(graph, node, visible_list, stack):

    for neighbour in list(graph.neighbors(node)):
        if not neighbour in visible_list:
            global start_node
            if neighbour == start_node:
                pass
            else:
                visible_list.append(neighbour)
                visible_list, stack = dfs(graph, neighbour, visible_list, stack)
    stack.append(node)
    return visible_list, stack


def dfs2(graph, node, visible_list):

    for neighbour in list(graph.neighbors(node)):
        if not neighbour in visible_list:
            global start_node
            if neighbour == start_node:
                pass
            visible_list.append(neighbour)
            visible_list = dfs2(graph, neighbour, visible_list)

    return visible_list


if __name__ == '__main__':

    first_graph = nx.DiGraph()
    first_graph.add_nodes_from(NODES)
    first_graph.add_edges_from(EDGES)

    visible_list = []
    stack = []

    for node in first_graph.nodes():
        if not node in visible_list:
            start_node = node
            visible_list.append(start_node)
            visible_list, stack = dfs(first_graph, start_node, visible_list, stack)

    print(visible_list, stack)

    REVERSE_EDGES = [(item[1], item[0]) for item in EDGES]
    second_graph = nx.DiGraph()
    second_graph.add_nodes_from(NODES)
    second_graph.add_edges_from(REVERSE_EDGES)

    stack.reverse()
    visible_list = []

    scc = []
    while stack.__len__() > 0:
        node = stack[0]
        visible_list = []
        visible_list = dfs2(second_graph, node, visible_list)
        if visible_list.__len__() == 0:
            visible_list.append(node)
        scc.append(visible_list)
        for item in visible_list:
            stack.remove(item)
            second_graph.remove_node(item)
    print(scc)
